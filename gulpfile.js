let gulp = require('gulp');

let del = require('del');

let rigger = require('gulp-rigger');

let less = require('gulp-less');
let plumber = require('gulp-plumber');
let postcss = require('gulp-postcss');
let autoprefixer = require('autoprefixer');
let mqpacker = require("css-mqpacker");
let sortCSSmq = require('sort-css-media-queries');
let minify = require('gulp-csso');
let rename = require('gulp-rename');
const runSequenceG4 = require('gulp4-run-sequence');

let uglify = require('gulp-uglify');

let imagemin = require('gulp-imagemin');

let svgmin = require('gulp-svgmin');
let svgstore = require('gulp-svgstore');

let runSequence = require('run-sequence');
let server = require('browser-sync');

gulp.task('clean', gulp.series(function(){
  return del('dist');
}));

gulp.task('copy', gulp.series(function() {
  return gulp.src([
    'src/assets/icons/**/*.*',
    '!src/assets/icons/svg-sprite/*.*',
    'src/assets/fonts/**/*.{woff,woff2}',
    'src/assets/video/**/*.{mp4,webm}',
    'src/assets/audio/**/*.{mp3,ogg,wav,aac}',
    'src/json/**/*.json',
  ], {
    base: 'src'
  })
  .pipe(gulp.dest('dist/'));
}));

gulp.task('rigger-html', gulp.series(function () {
    return gulp.src('src/html/*.html')
      .pipe(rigger())
      .pipe(gulp.dest('dist'));
}));

gulp.task('style', gulp.series(function() {
    return gulp.src('src/styles/styles.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(postcss([
      autoprefixer({overrideBrowserslist: ['last 4 version']}),
      mqpacker({
        sort: sortCSSmq.desktopFirst
      })
    ]))
    .pipe(gulp.dest('dist/styles'))
    .pipe(minify())
    .pipe(rename('styles.min.css'))
    .pipe(gulp.dest('dist/styles'));
}));

gulp.task('js-uglify', gulp.series(function() {
    return gulp.src('src/scripts/**/*.js')
  .pipe(plumber())
  .pipe(gulp.dest('dist/scripts'))
  .pipe(uglify())
  .pipe(rename(function(path){
    path.basename += '.min'
  }))
  .pipe(gulp.dest('dist/scripts'));
}));

gulp.task('images', gulp.series(function(){
    return gulp.src('src/assets/images/**/*.{png,jpg,jpeg,gif,svg}')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.mozjpeg({progressive: true})/*,
      imagemin.optipng({optimizationLevel: 5})*/
    ]))
    .pipe(gulp.dest('dist/assets/images'));
}));

gulp.task('symbols', gulp.series(function(){
    return gulp.src('src/assets/icons/svg-sprite/*.svg')
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(svgstore({
      inlineSvg: true
    }))
    .pipe(rename('symbols.svg'))
    .pipe(gulp.dest('dist/assets/icons'));
}));

gulp.task('html-library', gulp.series(function () {
    return gulp.src('src/html/library/blocks-includer.html')
      .pipe(rigger())
      .pipe(rename('_HTML-Library.html'))
      .pipe(gulp.dest('src/html/library/'));
}));

gulp.task('build', gulp.series(function(fn){
    return runSequenceG4('clean','copy','rigger-html','style','js-uglify', 'images', 'symbols','html-library', fn);
}));

let afterChange = setTimeout(function(){},0);
gulp.task('reload-serv', gulp.series(function(){
  clearTimeout(afterChange);
  afterChange = setTimeout(function(){
    server.reload();
  }, 500);
}));

gulp.task('serv', gulp.series(function() {
  server.init({
      server: "dist"
  });
  gulp.watch('./src/html/**/*.html', gulp.series('rigger-html'));
  gulp.watch('./src/styles/**/*.less', gulp.series('style'));
  gulp.watch('./src/scripts/**/*.js', gulp.series('js-uglify'));
  gulp.watch('./src/assets/images/**/*.{png,jpg,jpeg,gif,svg}', gulp.series('images'));
  gulp.watch('./src/assets/icons/svg-sprite/*.svg', gulp.series('symbols'));
  gulp.watch('./dist/**/*.*', gulp.series('reload-serv'));
}));

gulp.task( 'default', gulp.series( 'serv' ));
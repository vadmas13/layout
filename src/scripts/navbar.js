$(document).ready(function () {

    var widthSearch = $('.search__input ').width();


    function widthWindow(){
        return $(window).width();
    }



    function slide(id, widthSearch) {
        var leftScrolIcon;

        if (id === 'burger') {
            leftScrolIcon = $('.search__icon').css('left');

            $('#close').css({'opacity': 1, 'z-index': 20})
            $('#burger').css({'opacity': 0, 'z-index': 10})

            if (widthWindow() > 500){
                $('.scrollSearch').css({
                    'width' : '50px',
                    'left' : 240 - 104
                });
                $('.scrollSearch svg').css({
                    'left' : 240 - 104
                });
                $('.menu').css({'width': '240px'});
            }else {
                $('.scrollSearch').css({
                    'width' : '50px',
                    'left' : 192 - 12
                });
                $('.scrollSearch svg').css({
                    'left' : 192 - 12
                });
                $('.menu').css({'width': '192px'});
                $('.menuList__item').css({'opacity': 1});
            }
            $('.menuList__text').css({'opacity': 1})
            $('.menu__title').css({'opacity': 1})

        } else {
            $('#burger').css({'opacity': 1, 'z-index': 20})
            $('#close').css({'opacity': 0, 'z-index': 10})

            if (widthWindow() > 500) {
                $('.scrollSearch').css({
                    'width' : widthSearch,
                    'left' : 0
                });
                $('.scrollSearch svg').css({
                    'left' : leftScrolIcon
                });
                $('.menu').css({'width': '104px'});
            }
            else {
                $('.scrollSearch').css({
                    'width' : widthSearch,
                    'left' : 0
                });
                $('.scrollSearch svg').css({
                    'left' : leftScrolIcon
                });
                $('.menu').css({'width': '12px'});
                $('.menuList__item').css({'opacity': 0});
            }
            $('.menuList__text').css({'opacity': 0})
            $('.menu__title').css({'opacity': 0})
        }
    }

    $('.menu__nav').click(function () {
        var id = this.id;
        slide(id, widthSearch)
    })

    $('.menuList__link').click(function (e) {

        e.preventDefault();
        var liElem = this.closest('li');
        var title = $(liElem).data('title');
        $('.menuList__item').removeClass('menuList__item_active');
        $(liElem).addClass('menuList__item_active');
        $('#headerPageTitle').text(title);
        if (widthWindow() <= 1169) slide('', widthSearch);
    })


});

$(document).ready(function () {

    function setWidthSearch(leftOffset, searchPaddingLeft){
        var windowWidth  = $(window).width();
        var offset;
        if(windowWidth > 50) offset = 50;
        else offset = 0;
        return $(window).width() - leftOffset - offset - searchPaddingLeft;
    }


    var offsetSearch = $('.search').offset().top;
    var searchPaddingLeft = 100;
    var searchIconLeft =  parseInt($('.search__icon').css('left'));

    $('#searchInput').on('focus',function(){
        $('.search__icon').css('color', '#7F7F7F');
    });

    $('#searchInput').on('focusout',function(){
        $('.search__icon').css('color', '#D6D6D6');
    });

    $(window).scroll(function(){
        var scrollTopSearch = $(document).scrollTop() + 50;
        var leftOffset = $('.menu').width() + 48 - searchPaddingLeft + 10;
        if(scrollTopSearch > offsetSearch){
            $('.search').css({
                'position': 'fixed',
                'left': leftOffset,
                'padding-left' : searchPaddingLeft
            });
            $('.search__input').css({
                'width': setWidthSearch(leftOffset, searchPaddingLeft)
            });
            $('.search__icon').css({
                'left' : searchIconLeft + searchPaddingLeft
            })
            $('.shops').css({
                'margin-top' : $('.search').height() +
                    parseInt($('.search').css('padding-top'))  +
                    parseInt($('.search').css('padding-bottom'))
            })
        }else{
            $('.shops').css({
                'margin-top' : 0
            })
            $('.search').css({
                'position': 'relative',
                'left': '0px',
                'padding-left' : 0
            });
            $('.search__input').css({
                'width': '100%'
            });
            $('.search__icon').css({
                'left' : searchIconLeft
            })

        }
    })
})